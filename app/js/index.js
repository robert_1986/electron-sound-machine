"use strict";

var ipc = require("ipc-renderer");
var remote = require("remote");
var app = remote.require("app");
var tray = remote.require("tray");
var menu = remote.require("menu");
var menuItem = remote.require("menu-item");
var path = require("path");

var trayIcon = null;

if (process.platform === "darwin") {
    trayIcon = new tray(path.join(__dirname, "img/tray-iconTemplate.png"));
} else {
    trayIcon = new tray(path.join(__dirname, "img/tray-icon-alt.png"));
}

var menuTemplate = [
    {
        label: "Sound Machine",
        enabled: false
    },
    {
        type: 'separator'
    },
    {
        label: "Settings",
        click: function() {
            ipc.send("open-settings-window");
        }
    },
    {
        label: "Quit",
        click: function() {
            ipc.send("close-main-window");
        }
    }
];

var trayMenu = menu.buildFromTemplate(menuTemplate);
trayIcon.setContextMenu(trayMenu);

var soundButtons = document.querySelectorAll(".button-sound");

for (var i = 0; i < soundButtons.length; i++) {
    var soundButton = soundButtons[i];
    var soundName = soundButton.attributes["data-sound"].value;

    prepareButton(soundButton, soundName);
}

function prepareButton(el, sound) {
    el.querySelector("span").style.backgroundImage = 'url("img/icons/' + sound + '.png")';

    var audio = new Audio(__dirname + "/wav/" + sound + ".wav");
    el.addEventListener("click", function() {
        audio.currentTime = 0;
        audio.play();
    });
}

var closeEl = document.querySelector(".close");
closeEl.addEventListener("click", function() {
    ipc.send("close-main-window");
});

ipc.on("global-shortcut", function(channel, arg) {
    var event = new MouseEvent("click");
    console.log(channel, arg);
    soundButtons[arg].dispatchEvent(event);
});

var settingsEl = document.querySelector(".settings");
settingsEl.addEventListener("click", function() {
    ipc.send("open-settings-window");
});


var rightClickMenu = new menu();

rightClickMenu.append(new menuItem({
    label: "Toggle Developer Tools",
    accelerator: (function() {
        return process.platform == "darwin" ? "Alt+Command+I" : "Ctrl+Shift+I";
    })(),
    click: function(item, focusedWindow) {
      if (focusedWindow) {
        focusedWindow.toggleDevTools();
    }
    }
}));

rightClickMenu.append(new menuItem({
    type: "separator"
}));

rightClickMenu.append(new menuItem({
    label: "Menu Item 1",
    click: function() {
        alert("Clicked: Menu Item 1");
    }
}));

rightClickMenu.append(new menuItem({
    label: "Menu Item 2",
    click: function() {
        alert("Clicked: Menu Item 2");
    }
}));

window.addEventListener("contextmenu", function (e) {
  e.preventDefault();
  rightClickMenu.popup(remote.getCurrentWindow());
}, false);

var template = [
    {
        label: app.getName(),
        submenu: [
            {
                label: 'Quit',
                accelerator: 'Command+Q',
                click: function() { app.quit(); }
            }
        ]
    },
    {
        label: 'Custom Menu',
        submenu: [
            {
                label: 'Toggle Developer Tools',
                click: function(item, focusedWindow) {
                  if (focusedWindow) {
                    focusedWindow.toggleDevTools();
                    }
                }
            },
            {
                type: "separator"
            },
            {
                label: "Menu Item 1",
                click: function() {
                    alert("Clicked: Menu Item 1");
                }
            },
            {
                label: "Menu Item 2",
                click: function() {
                    alert("Clicked: Menu Item 2");
                }
            }
        ]
    }
];

var m = menu.buildFromTemplate(template);
menu.setApplicationMenu(m);
