"use strict";

var nconf = require("nconf").file({file: getUserHome() + "/.sound-machine-config.json"});

function saveSettings(key, value) {
    nconf.set(key, value);
    nconf.save();
}

function readSettings(key) {
    nconf.load();
    return nconf.get(key);
}

function getUserHome() {
    return process.env[(process.platform == 'win32') ? "USERHOME" : "HOME"];
}

module.exports = {
    saveSettings: saveSettings,
    readSettings: readSettings
};
