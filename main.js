"use strict";

var app = require("app");
var browserWindow = require("browser-window");
var ipc = require("ipc-main");
var globalShortcut = require("global-shortcut");
var config = require('./configuration');

var mainWindow = null;

app.on("ready", function() {
    if (!config.readSettings("shortcutKeys")) {
        config.saveSettings("shortcutKeys", ["ctrl", "shift"]);
    }

    mainWindow = new browserWindow({
        frame: false,
        height: 700,
        width: 368,
        resizable: false
    });

    mainWindow.loadURL("file://" + __dirname + "/app/index.html");

    setGlobalShortcuts();
});

function setGlobalShortcuts() {
    globalShortcut.unregisterAll();

    var shortcutKeySetting = config.readSettings("shortcutKeys");
    var shortcutPrefix = shortcutKeySetting.length === 0 ? "" : shortcutKeySetting.join("+") + "+";

    globalShortcut.register(shortcutPrefix + "1", function() {
        mainWindow.webContents.send("global-shortcut", 0);
    });

    globalShortcut.register(shortcutPrefix + "2", function() {
        mainWindow.webContents.send("global-shortcut", 1);
    });

    globalShortcut.register(shortcutPrefix + "3", function() {
        mainWindow.webContents.send("global-shortcut", 2);
    });

    globalShortcut.register(shortcutPrefix + "4", function() {
        mainWindow.webContents.send("global-shortcut", 3);
    });

    globalShortcut.register(shortcutPrefix + "5", function() {
        mainWindow.webContents.send("global-shortcut", 4);
    });

    globalShortcut.register(shortcutPrefix + "6", function() {
        mainWindow.webContents.send("global-shortcut", 5);
    });

    globalShortcut.register(shortcutPrefix + "7", function() {
        mainWindow.webContents.send("global-shortcut", 6);
    });

    globalShortcut.register(shortcutPrefix + "8", function() {
        mainWindow.webContents.send("global-shortcut", 7);
    });


}

ipc.on("close-main-window", function() {
    app.quit();
});

var settingsWindow = null;

ipc.on("open-settings-window", function() {
    if (settingsWindow) {
        return;
    }

    settingsWindow = new browserWindow({
        frame: false,
        resizable: false,
        height: 210,
        width: 200
    });

    settingsWindow.loadURL("file://" + __dirname + "/app/settings.html");

    settingsWindow.on("closed", function() {
        settingsWindow = null;
    });
});

ipc.on("close-settings-window", function() {
    if (settingsWindow) {
        settingsWindow.close();
    }
});

ipc.on("set-global-shortcuts", function() {
    setGlobalShortcuts();
});
